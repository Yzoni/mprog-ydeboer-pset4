package nl.yrck.mprog_to_dolist;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import nl.yrck.mprog_to_dolist.storage.DBHelper;
import nl.yrck.mprog_to_dolist.storage.Todo;

public class MainActivity extends AppCompatActivity {
    EditText editTextAdd;
    Button buttonAdd;
    Toolbar toolbar;
    LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    List<Todo> todos;

    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.reycler_view);
        buttonAdd = (Button) findViewById(R.id.add_todo_button);
        editTextAdd = (EditText) findViewById(R.id.add_todo_text);

        setSupportActionBar(toolbar);

        buttonAdd.setOnClickListener(view -> addTodo());

        todos = getTodos();

        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new RecyclerAdapter.SimpleDividerItemDecoration(this));
        recyclerAdapter = new RecyclerAdapter(todos, getApplicationContext());
        recyclerAdapter.setOnItemClickListener(new RecyclerAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View view) {
                Long tag = (Long) view.getTag();
                toggleTodoStatus(tag);
                updateRecycler();
            }

            @Override
            public void onItemLongClick(int position, View view) {
                Long tag = (Long) view.getTag();
                DBHelper dbHelper = new DBHelper(getApplicationContext());
                Todo todo = dbHelper.getTodo(tag);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Delete")
                        .setMessage("Delete todo " + todo.getName() + "?")
                        .setCancelable(true)
                        .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss())
                        .setPositiveButton("Delete", (dialog, which) -> deleteTodo(tag));
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        recyclerView.setAdapter(recyclerAdapter);
    }

    private void updateRecycler() {
        todos.clear();
        todos.addAll(getTodos());
        recyclerAdapter.notifyDataSetChanged();
    }

    private Todo toggleTodoStatus(long todoId) {
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        Todo todo = dbHelper.getTodo(todoId);
        if (todo.getStatus() == Todo.STATUS_CHECKED) {
            todo.setStatus(Todo.STATUS_UNCHECKED);
        } else {
            todo.setStatus(Todo.STATUS_CHECKED);
        }
        dbHelper.updateTodo(todo);
        dbHelper.close();
        return todo;
    }

    private List<Todo> getTodos() {
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        List<Todo> todos = dbHelper.getAllToDos();
        dbHelper.close();
        return todos;
    }

    private void addTodo() {
        String name = editTextAdd.getText().toString();
        if (!name.equals("")) {
            DBHelper dbHelper = new DBHelper(getApplicationContext());
            dbHelper.createTodo(new Todo(name, 0));
            editTextAdd.setText("");
            dbHelper.close();
            updateRecycler();
        }
    }

    private void deleteTodo(long todoId) {
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        dbHelper.deleteTodo(todoId);
        dbHelper.close();
        updateRecycler();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_about) {
            Util.aboutDialog(this);
        }

        return super.onOptionsItemSelected(item);
    }
}
