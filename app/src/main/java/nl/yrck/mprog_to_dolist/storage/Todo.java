package nl.yrck.mprog_to_dolist.storage;

public class Todo {

    public final static int STATUS_UNCHECKED = 0;
    public final static int STATUS_CHECKED = 1;

    private long id;
    private String name;
    private int status;
    private String created_at;

    public Todo(String name, int status) {
        this.name = name;
        this.status = status;
    }

    public Todo(int id, String name, int status, String created_at) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.created_at = created_at;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
