package nl.yrck.mprog_to_dolist.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "TODO";

    private static final String TABLE_TODO = "table_todo";
    private static final String COLUMN_TODO_ID = "id";
    private static final String COLUMN_TODO_NAME = "name";
    private static final String COLUMN_TODO_STATUS = "status";
    private static final String COLUMN_TODO_CREATEDAT = "created_at";

    private static final String CREATE_TABLE_TODO = "CREATE TABLE "
            + TABLE_TODO + "(" + COLUMN_TODO_ID + " INTEGER PRIMARY KEY," + COLUMN_TODO_NAME
            + " TEXT," + COLUMN_TODO_STATUS + " INTEGER," + COLUMN_TODO_CREATEDAT
            + " DATETIME" + ")";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_TODO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_TODO);
    }

    public void close() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public long createTodo(Todo todo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_TODO_NAME, todo.getName());
        values.put(COLUMN_TODO_STATUS, todo.getStatus());
        values.put(COLUMN_TODO_CREATEDAT, getDateTime());

        return db.insert(TABLE_TODO, null, values);
    }

    public Todo getTodo(long todoId) {
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_TODO + " WHERE " + COLUMN_TODO_ID + " = " + todoId;

        Cursor c = db.rawQuery(query, null);

        if (c != null)
            c.moveToFirst();

        Todo todo = new Todo(
                c.getInt(c.getColumnIndex(COLUMN_TODO_ID)),
                c.getString(c.getColumnIndex(COLUMN_TODO_NAME)),
                c.getInt(c.getColumnIndex(COLUMN_TODO_STATUS)),
                c.getString(c.getColumnIndex(COLUMN_TODO_CREATEDAT))
        );
        c.close();
        return todo;
    }

    public List<Todo> getAllToDos() {
        List<Todo> todos = new ArrayList<>();
        String query = "SELECT  * FROM " + TABLE_TODO;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst()) {
            do {
                todos.add(new Todo(
                        c.getInt(c.getColumnIndex(COLUMN_TODO_ID)),
                        c.getString(c.getColumnIndex(COLUMN_TODO_NAME)),
                        c.getInt(c.getColumnIndex(COLUMN_TODO_STATUS)),
                        c.getString(c.getColumnIndex(COLUMN_TODO_CREATEDAT))
                ));

            } while (c.moveToNext());
        }

        return todos;
    }

    public int updateTodo(Todo todo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_TODO_NAME, todo.getName());
        values.put(COLUMN_TODO_STATUS, todo.getStatus());

        return db.update(TABLE_TODO, values, COLUMN_TODO_ID + " = ?",
                new String[]{String.valueOf(todo.getId())});
    }

    public void deleteTodo(long todoId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TODO, COLUMN_TODO_ID + " = ?",
                new String[]{String.valueOf(todoId)});
    }
}
